/**
 * Word Metronome
 * Copyright 2021 Hamm Software & Samantha Landa
 * https://www.hamm.software
 */
var AudioLoader = {
    
    'AUDIO_ENGINE_HOWLER': 'howler',
    'AUDIO_ENGINE_WEB_AUDIO': 'webAudio',
    
    'audioEngine': null,
    
    'howlerFileCache': {},
    'howlerFileCacheExtra': {},
    
    'webAudioArrayBufferCache': {},
    'webAudioBufferCache': {},
    
    'countFilesLoaded': 0,
    'countFilesTotal': 0,
    
    'clickSpriteData': {},
    'clickSpriteFileMap': {},
    
    'loadCallback': null,
    
    /**
     * Sets the audio engine, options are to use Howler or the web audio API.
     * 
     * @author Peter Hamm
     * @param String audioEngine Optional, uses "howler" by default. Options are: AudioLoader.AUDIO_ENGINE_HOWLER or AudioLoader.AUDIO_ENGINE_WEB_AUDIO
     * @return void
     */
    'setAudioEngine': function(audioEngine)
    {
        
        if(!audioEngine || audioEngine.length == 0 || (audioEngine != AudioLoader.AUDIO_ENGINE_HOWLER && audioEngine != AudioLoader.AUDIO_ENGINE_WEB_AUDIO))
        {
            
            audioEngine = AudioLoader.AUDIO_ENGINE_HOWLER;
            
        }
        
        AudioLoader.audioEngine = audioEngine;
        
        WordMetronome.log("Using the audio engine \"" + AudioLoader.audioEngine + "\".");
        
        if(AudioLoader.audioEngine == AudioLoader.AUDIO_ENGINE_HOWLER)
        {
            
            AudioLoader.howlerInit();
            
        }
        
    },
    
    /**
     * Plays the specified file at the specified time.
     * 
     * @author Peter Hamm
     * @param String filePath
     * @param int time
     * @param int beatIndex
     * @returns boolean
     */
    'playFile': function(filePath, time, beatIndex)
    {
        
        if(AudioLoader.audioEngine == AudioLoader.AUDIO_ENGINE_HOWLER)
        {
                        
            return AudioLoader.howlerPlayFile(filePath, time, beatIndex);
            
        }
        else if(AudioLoader.audioEngine == AudioLoader.AUDIO_ENGINE_WEB_AUDIO)
        {
            
            return AudioLoader.webAudioCreateAndQueueBufferForFile(filePath, time);
            
        }
        
        return false;
        
    },
    
    /**
     * Creates an audio buffer for the specified file and queues it at the specified time.
     * 
     * @author Peter Hamm
     * @param String filePath
     * @param int beatIndex
     * @return boolean
     */
    'howlerPlayFile': function(filePath, time, beatIndex)
    {
        
        // Making use of the audioContext's timer function is more performant that working with
        // timeouts or intervals, for iPhones and older devices.
        {
        
            if(!AudioLoader.howlerFileCache[filePath] && !AudioLoader.howlerFileCacheExtra[filePath])
            {
                
                return false;

            }
            
            WordMetronome.webAudioNextOscillator = WordMetronome.audioContext.createOscillator();

            WordMetronome.webAudioNextOscillator.connect(WordMetronome.audioContext.destination);
            WordMetronome.webAudioNextOscillator.frequency.value = 1;

            WordMetronome.webAudioNextOscillator.start(time);
            WordMetronome.webAudioNextOscillator.stop(time + 0.00001);

            WordMetronome.webAudioNextOscillator.onended = function()
            {

                var spriteIndex = (AudioLoader.clickSpriteFileMap[filePath] ? beatIndex + "" : undefined);

                if(AudioLoader.howlerFileCache[filePath])
                {

                    AudioLoader.howlerFileCache[filePath].play(spriteIndex);   

                }
                else if(AudioLoader.howlerFileCacheExtra[filePath])
                {

                    AudioLoader.howlerFileCacheExtra[filePath].play(spriteIndex);   

                }

            };
            
            return true;
            
        }
        
        // Slow version, will lag on iPhones and older devices.
        /*
        var nextPlayTime = time - WordMetronome.getAudioContext().currentTime;
        
        if(nextPlayTime > WordMetronome.beatTime)
        {
        
            if(!AudioLoader.howlerFileCache[filePath] && !AudioLoader.howlerFileCacheExtra[filePath])
            {
                
                return false;

            }
            AudioLoader.howlerNextPlay = setTimeout(function() {

                var spriteIndex = (AudioLoader.clickSpriteFileMap[filePath] ? beatIndex + "" : undefined);
                
                if(AudioLoader.howlerFileCache[filePath])
                {
                                        
                    AudioLoader.howlerFileCache[filePath].play(spriteIndex);   
                    
                }
                else if(AudioLoader.howlerFileCacheExtra[filePath])
                {
                    
                    AudioLoader.howlerFileCacheExtra[filePath].play(spriteIndex);   
                    
                }

            }, nextPlayTime);
            
            return true;
            
        }
        */
        
        return false;
        
    },
    
    /**
     * Creates an audio buffer for the specified file and queues it at the specified time.
     * 
     * @author Peter Hamm
     * @param String filePath
     * @return boolean
     */
    'webAudioCreateAndQueueBufferForFile': function(filePath, time)
    {
        
        if(AudioLoader.webAudioBufferCache[filePath])
        {
            
            WordMetronome.webAudioQueuePlaybackFileBuffer(AudioLoader.webAudioBufferCache[filePath], time);

            return true;

        }
        
        if(AudioLoader.webAudioArrayBufferCache[filePath])
        {
            
            var audioContext = WordMetronome.getAudioContext();
            
            if(audioContext != null)
            {
                
                audioContext.decodeAudioData(AudioLoader.webAudioArrayBufferCache[filePath], function(buffer){
                    
                    AudioLoader.webAudioBufferCache[filePath] = buffer;
                    
                    WordMetronome.webAudioQueuePlaybackFileBuffer(AudioLoader.webAudioBufferCache[filePath], time);
                    
                });
                
            }
            
            return true;
            
        }
                
        return false;
                
    },
    
    /**
     * Special initialization function required for howler.
     * 
     * @author Peter Hamm
     * @returns void
     */
    'howlerInit': function()
    {
      
        AudioLoader.howlerPreloadFile('click/click-low.mp3', true);
        AudioLoader.howlerPreloadFile('click/click-high.mp3', true);
        
    },
        
    /**
     * Loads the audio file at the specified path and
     * caches it for use with the howler audio engine. 
     * Triggers the optional AudioLoader.loadCallback, if it is specified.
     * 
     * @author Peter Hamm
     * @param String filePath
     * @param boolean asExtra Optional, default: false
     * @return void
     */
    'howlerPreloadFile': function(filePath, asExtra)
    {
        
        if((!asExtra && AudioLoader.howlerFileCache[filePath]) || (asExtra && AudioLoader.howlerFileCacheExtra[filePath]))
        {
                        
            // Do not re-cache duplicates.
            
            return;
            
        }
        
        var filePathRelative = WordMetronome.baseUrl + 'audio/' + filePath;
        
        var spriteData = null;
                
        if(AudioLoader.clickSpriteFileMap[filePath])
        {
            
            // This file is an audio sprite!
            
            spriteData = AudioLoader.clickSpriteData[AudioLoader.clickSpriteFileMap[filePath]].edges;
            
        }
        
        var doPreload = false;
                
        var howl = new Howl(

            {
                
                'src': [filePathRelative],
                'preload': doPreload,
                'usingWebAudio': false, 
                'webAudio': false, 
                'html5': true, 
                'mute': false, 
                'loop': false, 
                'sprite': spriteData,
                'volume': 1,
                
                'pool': 200
                
            }

        );

        if(asExtra)
        {
            
            AudioLoader.howlerFileCacheExtra[filePath] = howl;
            
        }
        else
        {
            
            AudioLoader.howlerFileCache[filePath] = howl;

            AudioLoader.howlerFileCache[filePath].on('end', function()
            {

                //console.log('end');

                //AudioLoader.howlerFileCache[filePath].pause();

            });

            if(doPreload)
            {
                
                AudioLoader.howlerFileCache[filePath].on('load', function()
                {

                    AudioLoader.incrementCountFilesLoaded();

                });

                AudioLoader.howlerFileCache[filePath].on('loaderror', function()
                {

                    delete AudioLoader.howlerFileCache[filePath];

                    AudioLoader.incrementCountFilesLoaded();

                });
                
            }
            else
            {
                
                AudioLoader.incrementCountFilesLoaded();
                
            }
            
            delete howl;
            
        }

    },
    
    /**
     * Loads the audio file at the specified path and
     * caches the buffer. Triggers the optional AudioLoader.loadCallback, if it is specified.
     * 
     * @author Peter Hamm
     * @param String filePath
     * @return void
     */
    'webAudioCacheFileToArrayBuffer': function(filePath)
    {
                
        if(AudioLoader.webAudioArrayBufferCache[filePath])
        {
                        
            // Do not re-cache duplicates.
            
            return;
            
        }
        
        AudioLoader.webAudioArrayBufferCache[filePath] = 1;
        
        var filePathRelative = WordMetronome.baseUrl + 'audio/' + filePath;
        
        var xhrOverride = new XMLHttpRequest();
        xhrOverride.responseType = 'arraybuffer';

        jQuery.ajax({
            
            url: filePathRelative,
            method: 'GET',
            
            xhr: function() 
            {
                
                return xhrOverride;
                
            }
            
        }).then(function(arrayBuffer, status, xhr) {
                        
            if(xhr.status != 200 || !arrayBuffer || arrayBuffer == -1)
            {
                
                delete AudioLoader.webAudioArrayBufferCache[filePath];
                
            }
            else
            {

                AudioLoader.webAudioArrayBufferCache[filePath] = arrayBuffer;

            }

            AudioLoader.incrementCountFilesLoaded();

        });
        
    },
    
    /**
     * Increments the number of loaded / buffered audio files by one.
     * Once all files from the definitions have been loaded (or failed loading), 
     * the AudioLoader.loadCallback() will be triggered, if defined.
     * 
     * @author Peter Hamm
     * @return void
     */
    'incrementCountFilesLoaded': function()
    {
        
        ++AudioLoader.countFilesLoaded;
        
        if(AudioLoader.countFilesLoaded == AudioLoader.countFilesTotal)
        {
            
            WordMetronome.log('All audio files loaded.');
            
        }
        
        AudioLoader.checkCountsForCallback();
        
    },
    
    /**
     * Checks whether all definition and audio files have been loaded or not.
     * Will fire the function AudioLoader.loadCallback if that's the case, and if it is defined.
     * 
     * @author Peter Hamm
     * @returns void
     */
    'checkCountsForCallback': function()
    {
        
        if(AudioLoader.countFilesLoaded == AudioLoader.countFilesTotal && Definitions.isLoaded())
        {
            
            if(typeof AudioLoader.loadCallback === "function")
            {
                
                AudioLoader.loadCallback();
                
            }

        }
        
    },
    
    /**
     * Caches all audio files from the definitions into audio buffers.
     * 
     * @author Peter Hamm
     * @param Object definitionList
     * @param Function callback Optional, fired when all files have been loaded.
     * @return {undefined}
     */
    'cacheAllFiles': function(definitionList, callback)
    {
        
        var fileList = [];
        
        if(typeof definitionList === "undefined" || !definitionList)
        {
            
            console.error("Invalid definition list.");
            
        }
        
        AudioLoader.loadCallback = callback;
        
        var definitionKeys = Object.keys(Definitions.definitions);
        
        for(var i = 0; i < definitionKeys.length; ++i)
        {
            
            var currentDefinition = Definitions.definitions[definitionKeys[i]];
            
            if(currentDefinition.signatures && currentDefinition.signatures.length > 0)
            {
                
                for(var j = 0; j < currentDefinition.signatures.length; ++j)
                {
                    
                    var currentSignature = currentDefinition.signatures[j];
                    
                    if(AudioLoader.audioEngine == AudioLoader.AUDIO_ENGINE_HOWLER && currentSignature.clickSprite && currentSignature.clickSprite.file)
                    {

                        // When using Howler, we can make use of audio sprites for improved performance.
                        
                        var clickSpriteKey = definitionKeys[i] + '-' + j;
                        
                        if(!AudioLoader.clickSpriteData[clickSpriteKey])
                        {
                            
                            AudioLoader.clickSpriteData[clickSpriteKey] = currentSignature.clickSprite;
                            AudioLoader.clickSpriteFileMap[currentSignature.clickSprite.file] = clickSpriteKey;
                            
                        }
                        
                        if(!fileList.includes(currentSignature.clickSprite.file))
                        {

                            fileList.push(currentSignature.clickSprite.file);

                        }

                    }
                    else
                    {
                    
                        for(var k = 0; k < currentSignature.clicks.length; ++k)
                        {

                            if(currentSignature.clicks[k].type == Definitions.FILE && currentSignature.clicks[k].file && currentSignature.clicks[k].file.length > 0)
                            {

                                if(!fileList.includes(currentSignature.clicks[k].file))
                                {

                                    fileList.push(currentSignature.clicks[k].file);

                                }

                            }

                        }
                        
                    }

                }
                
            }
            
        }
        
        AudioLoader.countFilesTotal = fileList.length;
        
        if(Definitions.isLoaded())
        {

            WordMetronome.log('Loading a total of ' + AudioLoader.countFilesTotal + ' unique audio files...');

            for(var i = 0; i < fileList.length; ++i)
            {
                
                if(AudioLoader.audioEngine == AudioLoader.AUDIO_ENGINE_HOWLER)
                {
                    
                    AudioLoader.howlerPreloadFile(fileList[i]);
                    
                }
                else if(AudioLoader.audioEngine == AudioLoader.AUDIO_ENGINE_WEB_AUDIO)
                {
                    
                    AudioLoader.webAudioCacheFileToArrayBuffer(fileList[i]);
                    
                }

            }

        }
                    
    }
    
};
