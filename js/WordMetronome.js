/**
 * Word Metronome
 * Copyright 2021 Hamm Software & Samantha Landa
 * https://www.hamm.software
 */
var WordMetronome = {
    
    // Obfuscator: https://obfuscator.io/
    // Numeric: https://tools.bartlweb.net/jspacker/
        
    'debugMode': false,
    
    'baseUrl': './',
    
    'audioContext': null,
    'timerId': null,
    
    'clock': null,
    'nextBeat': null,
    'nextBeatTime': null,
    
    'selectedStyle': '',
    'selectedBpm': 0,
    'selectedSignatureIndex': 0,
    'selectedSignatureNumerator': 0,
    'selectedSignatureDenominator': 0,
    'beatTime': 0,
    'beatIndex': 0,
    
    'webAudioNextPlaybackFile': null,
    'webAudioNextOscillator': null,
    
    'clicks': [],
    
    /**
     * Initializes the metronome with the specified audio engine.
     * The audio engine "webAudio" has the disadvantage that it cannot play sound when an iOS device is in silent mode.
     * The audio engine "Howler" works fine for this.
     * 
     * @author Peter Hamm
     * @param String audioEngine Optional, used "howler" by default. Options are: AudioLoader.AUDIO_ENGINE_HOWLER or AudioLoader.AUDIO_ENGINE_WEB_AUDIO
     * @return void
     */
    'init': function(audioEngine) 
    {
        
        document.addEventListener('contextmenu', event => event.preventDefault());
        
        AudioLoader.setAudioEngine(audioEngine);
        
        WordMetronome.setLoading(true);
        
        WordMetronome.clock = jQuery("#clock");
        WordMetronome.nextBeat = jQuery("#nextBeat");
        
        jQuery('#a-troubleshooting').click(WordMetronome.toggleTroubleshooting);
        jQuery('#a-terms-conditions').click(WordMetronome.toggleTermsAndConditions);
        
        jQuery('.btn-close-modal, .btn-close-modal-x').click(WordMetronome.closeModals);
        
        jQuery("#btn-play").click(function() 
        {

            WordMetronome.startMetronome();

        });

        jQuery("#btn-stop").click(function() 
        {

            WordMetronome.stopMetronome();

        });
        
        jQuery('#metronome-animation-row').click(function() 
        {
            
            WordMetronome.toggleMetronome();
            
        });
        
        jQuery('body').keydown(function(e)
        {
            
            if(e.keyCode == 32)
            {
                
                e.preventDefault();
                e.stopPropagation();
                
                return false;
                
                
            }
            
        }).keyup(function(e)
        {
            
            if(e.keyCode == 32)
            {
                
                e.preventDefault();
                e.stopPropagation();
                
                WordMetronome.toggleMetronome();
                
                return false;
                
                
            }
            
        });
        
        jQuery('#selector-style').change(WordMetronome.onStyleChange);
        jQuery('#selector-signature, #selector-bpm').change(WordMetronome.updateValuesFromSelection);
        jQuery('[name="selector-animation"]').change(MetronomeAnimation.onMetronomeChange);
                                
        MetronomeAnimation.onMetronomeChange();
                                
        Definitions.loadDefinitions(WordMetronome.onLoadComplete);
        
        if(WordMetronome.debugMode)
        {
            
            jQuery('.debug-info').css('display', 'inline-block');
            
        }
        
        MetronomeAnimation.init();

    },
    
    /**
     * Toggles the display of the terms & conditions modal.
     * 
     * @author Peter Hamm
     * @returns void
     */
    'toggleTermsAndConditions': function()
    {
        
        var modal = jQuery('#modal-terms-conditions');
        
        var isShown = modal.css('display') == 'block';
        
        modal.modal(isShown ? 'hide' : 'show');
        
    },
    
    /**
     * Toggles the display of the troubleshooting modal.
     * 
     * @author Peter Hamm
     * @returns void
     */
    'toggleTroubleshooting': function()
    {
        
        var modal = jQuery('#modal-troubleshooting');
        
        var isShown = modal.css('display') == 'block';
        
        modal.modal(isShown ? 'hide' : 'show');
        
    },
    
    /**
     * Closes all open modals.
     * 
     * @author Peter Hamm
     * @returns void
     */
    'closeModals': function()
    {
        
        jQuery('.modal').modal('hide');
        
    },
    
    /**
     * Tries to retrieve the audio context.
     * The audio context is only available after user interaction (e.g. a click).
     * 
     * @author Peter Hamm
     * @return Object
     */
    'getAudioContext': function()
    {
        
        if(WordMetronome.audioContext == null)
        {
            
            console.error("Audio context not yet available.");
            
            return null;
            
        }
        
        return WordMetronome.audioContext;
        
    },
    
    /**
     * Sets the loading status and enabled/disables user interactions accordingly.
     * 
     * @author Peter Hamm
     * @param bool isLoading
     * @return void
     */
    'setLoading': function(isLoading)
    {
        
        jQuery('#btn-play, #btn-stop, #selector-style, #selector-signature, #selector-bpm, [name="selector-animation"]').prop('disabled', isLoading);
        
        jQuery('#loading-indicator').css('display', isLoading ? 'block' : 'none');
                        
    },
    
    /**
     * Handles changes of the metronome style.
     * 
     * @author Peter Hamm
     * @return void
     */
    'onStyleChange': function()
    {
        
        var selectedStyle = jQuery('#selector-style').val();
        
        WordMetronome.buildInterfaceForDefinition(selectedStyle);
        
        MetronomeAnimation.onStyleChange();
        
        WordMetronome.log("Style changed to '" + selectedStyle + "'")
        
    },
    
    /**
     * Retrieves the currently selected style.
     * 
     * @author Peter Hamm
     * @returns String
     */
    'getSelectedStyle': function()
    {
        
        return WordMetronome.selectedStyle;
        
    },
    
    /**
     * Updates the metronome timing values from the current selection of BPM and signature.
     * 
     * @author Peter Hamm
     * @return void
     */
    'updateValuesFromSelection': function()
    {
                
        WordMetronome.selectedBpm = parseInt(jQuery('#selector-bpm').val());
        WordMetronome.selectedSignatureIndex = parseInt(jQuery('#selector-signature option:selected').attr('data-index'));
        WordMetronome.selectedSignatureNumerator = parseInt(jQuery('#selector-signature option:selected').attr('data-numerator'));
        WordMetronome.selectedSignatureDenominator =  parseInt(jQuery('#selector-signature option:selected').attr('data-denominator'));
        WordMetronome.selectedStyle = jQuery('#selector-style').val();
                
        // 1 beat = 1 quarter note. 
        // 1 quarter note = 1 metronome beat at 120 bpm
        // Example: 1 / 120 * 60 = 0.5 seconds / beat
        WordMetronome.beatTime = 1 / WordMetronome.selectedBpm * 60;
        
        if(WordMetronome.selectedSignatureDenominator == 1)
        {
            
            WordMetronome.beatTime *= 4;
            
        }
        else if(WordMetronome.selectedSignatureDenominator == 2)
        {
            
            WordMetronome.beatTime *= 2;
            
        }
        else if(WordMetronome.selectedSignatureDenominator == 8)
        {
            
            WordMetronome.beatTime /= 2;
            
        }
        else if(WordMetronome.selectedSignatureDenominator == 8)
        {
            
            WordMetronome.beatTime /= 4;
            
        }
        else if(WordMetronome.selectedSignatureDenominator == 16)
        {
            
            WordMetronome.beatTime /= 8;
            
        }
        else if(WordMetronome.selectedSignatureDenominator == 32)
        {
            
            WordMetronome.beatTime /= 16;
            
        }
        else if(WordMetronome.selectedSignatureDenominator == 64)
        {
            
            WordMetronome.beatTime /= 32;
            
        }
        
        if(WordMetronome.selectedSignatureNumerator < 1 || WordMetronome.selectedSignatureDenominator < 1 || WordMetronome.selectedSignatureDenominator > 64)
        {
            
            var error = "Invalid signature " + WordMetronome.selectedSignatureNumerator + '/' + WordMetronome.selectedSignatureDenominator + ' in style "' + jQuery('#selector-style option:selected').html() + '"!';
            
            toastr.error(error);
            
            throw new Error(error);
            
        }
        
        WordMetronome.clicks = Definitions.getClickForSignatureByIndex(WordMetronome.selectedStyle, WordMetronome.selectedSignatureIndex);
                
        WordMetronome.log("Updating beat time for " + WordMetronome.selectedBpm + " BPM and signature " + WordMetronome.selectedSignatureNumerator + "/" + WordMetronome.selectedSignatureDenominator + ": " + WordMetronome.beatTime + " seconds.");
        
    },
    
    'toggleMetronome': function()
    {
      
        if(WordMetronome.timerId == null)
        {
            
            WordMetronome.startMetronome();
            
        }
        else
        {
            
            WordMetronome.stopMetronome();
            
        }
        
    },
    
    /**
     * Starts the playback of the metronome.
     * 
     * @author Peter Hamm
     * @return void
     */
    'startMetronome': function()
    {
        
        if(WordMetronome.audioContext == null)
        {

            window.AudioContext = window.AudioContext || window.webkitAudioContext;

            WordMetronome.audioContext = new AudioContext();

            WordMetronome.nextBeatTime = WordMetronome.audioContext.currentTime;
            
            if(WordMetronome.debugMode)
            {
                
                setInterval(function() 
                { 

                    WordMetronome.clock.val(WordMetronome.audioContext.currentTime); 

                }, 100);

            }

        }

        if(WordMetronome.audioContext.state === 'suspended')
        {

            WordMetronome.audioContext.resume();

        };
		
        WordMetronome.stopMetronome();
           
        WordMetronome.scheduleBeat();  
        
        WordMetronome.log('Started the metronome.');
        
        jQuery('#btn-box-play').css('display', 'none');
        jQuery('#btn-box-stop').css('display', 'block');
        
        MetronomeAnimation.onMetronomePlay();
        
    },
    
    /**
     * Stops the playback of the metronome.
     * 
     * @author Peter Hamm
     * @return void
     */
    'stopMetronome': function()
    {
                
        if(WordMetronome.timerId != null)
        {
            
            if(WordMetronome.webAudioNextOscillator != null)
            {
                
                try
                {

                    WordMetronome.webAudioNextOscillator.stop(WordMetronome.audioContext.currentTime);
                    WordMetronome.webAudioNextOscillator.disconnect();

                    delete WordMetronome.webAudioNextOscillator;

                    WordMetronome.webAudioNextOscillator = null;

                }
                catch(e)
                {

                    WordMetronome.log("Webkit issue with stopping the oscillator on time.");

                }
                
            }
            
            if(WordMetronome.webAudioNextPlaybackFile != null)
            {
                
                try
                {
                    
                    WordMetronome.webAudioNextPlaybackFile.stop(WordMetronome.audioContext.currentTime);
                    WordMetronome.webAudioNextPlaybackFile.disconnect();
                    
                    delete WordMetronome.webAudioNextPlaybackFile;
                    
                    WordMetronome.webAudioNextPlaybackFile = null;

                }
                catch(e)
                {

                    WordMetronome.log("Webkit issue with stopping the playback file on time.");

                }
                
            }
            
            clearTimeout(WordMetronome.timerId);

            WordMetronome.timerId = null;

            // @to do: Put this at a place where it doesn't conflict with the last note after clicking 'stopMetronome'.
            WordMetronome.beatIndex = 0;

            WordMetronome.log('Stopped the metronome.');
            
            jQuery('#btn-box-stop').css('display', 'none');
            jQuery('#btn-box-play').css('display', 'block');
            
            MetronomeAnimation.onMetronomeStop();
            
        }
        
    },
    
    /**
     * Will log the specified data to the console in debug mode.
     * 
     * @author Peter Hamm
     * @param mixed data
     * @return void
     */
    'log': function(data)
    {
        
        if(WordMetronome.debugMode)
        {
            
            console.log(data);
            
        }
        
    },
    
    /**
     * Fired as soon as the definitions and their attached audio files
     * have been loaded completely.
     * 
     * @author Peter Hamm
     * @return void
     */
    'onLoadComplete': function()
    {
        
        WordMetronome.log('All definitions and audio files loaded.');
        
        var definitionLabels = Definitions.getDefinitionLabels();
                
        if(definitionLabels.length > 0)
        {

            var optionString = "";
            
            for(var i = 0; i < definitionLabels.length; ++i)
            {
                
                var isSelected = (Definitions.defautDefinition && Definitions.defautDefinition.length > 0 && Definitions.defautDefinition == definitionLabels[i].key) || (!Definitions.defautDefinition || Definitions.defautDefinition.length == 0); 

                optionString += '<option value="' + definitionLabels[i].key + '"' + (isSelected ? " selected" : "") + '>' + definitionLabels[i].label + "</option>";

            }

            jQuery('#selector-style').html(optionString);

            var firstStyle = jQuery('#selector-style').val();

            WordMetronome.buildInterfaceForDefinition(firstStyle);
            
            setTimeout(function() {
                
                WordMetronome.setLoading(false);
                
            }, 1000);
            
        }
        else
        {
            
            console.warn("No definitions loaded yet.");
            
        }
        
    },
    
    /**
     * Builds the interface for the definitions with the specified key.
     * 
     * @author Peter Hamm
     * @param String definitionKey
     * @return void
     */
    'buildInterfaceForDefinition': function(definitionKey)
    {
        
        //WordMetronome.stopMetronome();
        
        var definition = Definitions.getDefinition(definitionKey);
        
        if(definition == null || !definition.signatures || definition.signatures.length == 0 || !definition.beatRange || definition.beatRange.length == 0)
        {
            
            console.error("Invalid definition.");
            
            return;
            
        }
        
        var defaultBeat = (definition.defaultBeat && parseInt(definition.defaultBeat) > 0) ? definition.defaultBeat : null;
        var defaultSignature = (definition.defaultSignature && parseInt(definition.defaultSignature.numerator) > 0 && parseInt(definition.defaultSignature.denominator) > 0) ? definition.defaultSignature : null;
        
        var signatureString = "";
                    
        for(var i = 0; i < definition.signatures.length; ++i)
        {
            
            var isSelected = (defaultSignature == null && i == 0) || (defaultSignature != null && defaultSignature.numerator == definition.signatures[i].numerator && defaultSignature.denominator == definition.signatures[i].denominator);
            
            signatureString += '<option data-index="' + i + '" data-numerator="' + definition.signatures[i].numerator + '" data-denominator="' + definition.signatures[i].denominator + '" value="' + definition.signatures[i].numerator + "-" + definition.signatures[i].denominator + '"' + (isSelected ? " selected" : "") + '>' + definition.signatures[i].numerator + "/" + definition.signatures[i].denominator + (definition.signatures[i].title && definition.signatures[i].title.length > 0 ? " - \"" + definition.signatures[i].title + "\"" : "") + "</option>";
            
        }
        
        jQuery('#selector-signature').html(signatureString);
        
        var bpmString = "";
        
        for(var i = 0; i < definition.beatRange.length; ++i)
        {
            
            var isSelected = (defaultBeat == null && i == 0) || (defaultBeat != null && defaultBeat == definition.beatRange[i]);
                        
            bpmString += '<option data-index="' + i + '" value="' + definition.beatRange[i] + '"' + (isSelected ? " selected" : "") + '>' + definition.beatRange[i] + " BPM</option>";
            
        }
        
        jQuery('#selector-bpm').html(bpmString);
        
        WordMetronome.updateValuesFromSelection();
        
    },
    
    /**
     * Plays the next sound in the queue.
     * 
     * @author Peter Hamm
     * @param int time
     * @return void
     */
    'playSound': function(time) 
    {
                
        if(WordMetronome.beatIndex >= WordMetronome.selectedSignatureNumerator)
        {

            WordMetronome.beatIndex = 0;

        }
        
        var scheduleResult = false;
       
        var clickType = WordMetronome.clicks[WordMetronome.beatIndex].type;

        if(clickType == Definitions.CLICK || clickType == Definitions.CLICK_HIGH || clickType == Definitions.CLICK_LOW)
        {
        
            scheduleResult = WordMetronome.playClick(time, clickType);
            
        }
        else if(clickType == Definitions.FILE)
        {
            
            var clickFile = "";
            
            var clickSpriteKey = WordMetronome.selectedStyle + '-' + WordMetronome.selectedSignatureIndex;
            
            if(AudioLoader.clickSpriteData[clickSpriteKey] && AudioLoader.clickSpriteData[clickSpriteKey].file)
            {
                                
                clickFile = AudioLoader.clickSpriteData[clickSpriteKey].file;
                
            }
            else
            {
                
                clickFile = WordMetronome.clicks[WordMetronome.beatIndex].file;
                
            }
            
            scheduleResult = AudioLoader.playFile(clickFile, time, WordMetronome.beatIndex);
            
        }
        
        if(scheduleResult)
        {
            
            ++WordMetronome.beatIndex;
            
        }
        
    },
    
    /**
     * Queues the specified audio buffer to be played at the specified time.
     * 
     * @author Peter Hamm
     * @param Object buffer
     * @param int time
     * @returns void
     */
    'webAudioQueuePlaybackFileBuffer': function(buffer, time)
    {
        
        WordMetronome.webAudioNextPlaybackFile = WordMetronome.audioContext.createBufferSource();

        WordMetronome.webAudioNextPlaybackFile.buffer = buffer;
        WordMetronome.webAudioNextPlaybackFile.connect(WordMetronome.audioContext.destination);
        WordMetronome.webAudioNextPlaybackFile.start(time);
        
    },
        
    /**
     * Plays a click at the specified time, of the specified type.
     * 
     * @param int time
     * @param String type Optional, may be "click-high" or "click-low", default: "click-low".
     * @return boolean
     */
    'playClick': function(time, type)
    {
    
        if(AudioLoader.audioEngine == AudioLoader.AUDIO_ENGINE_HOWLER)
        {
            
            return AudioLoader.playFile('click/' + type + '.mp3', time);
            
        }
        else if(AudioLoader.audioEngine == AudioLoader.AUDIO_ENGINE_WEB_AUDIO)
        {
            
            return WordMetronome.webAudioPlayClick(time, type);
            
        }
        
        return false;
        
    },
    
    /**
     * Plays the synthetically generated click for the web audio engine.
     * 
     * @author Peter Hamm
     * @param int time
     * @param String type Optional, may be "click-high" or "click-low", default: "click-low".
     * @returns boolean
     */
    'webAudioPlayClick': function(time, type)
    {
                
        WordMetronome.webAudioNextOscillator = WordMetronome.audioContext.createOscillator();
        
        WordMetronome.webAudioNextOscillator.connect(WordMetronome.audioContext.destination);
        WordMetronome.webAudioNextOscillator.frequency.value = (typeof type !== 'undefined' && type == 'click-high' ? 400 : 250);
        
        WordMetronome.webAudioNextOscillator.start(time);
        WordMetronome.webAudioNextOscillator.stop(time + 0.05);
                
        return true;
        
    },
    
    /**
     * Schedules the next beat.
     * 
     * @author Peter Hamm
     * @return void
     */
    'scheduleBeat': function() 
    {
        
        while(WordMetronome.nextBeatTime <= WordMetronome.audioContext.currentTime + 0.1) 
        {

            WordMetronome.nextBeatTime += WordMetronome.beatTime;
            
            if(WordMetronome.debugMode)
            {
                
                WordMetronome.nextBeat.val(WordMetronome.nextBeatTime);
            
            }
            
            if(WordMetronome.nextBeatTime > WordMetronome.audioContext.currentTime)
            {
                
                WordMetronome.playSound(WordMetronome.nextBeatTime);
                
                MetronomeAnimation.scheduleClick(WordMetronome.nextBeatTime - WordMetronome.audioContext.currentTime);
            
            }
            
        }
        
        WordMetronome.timerId = window.setTimeout(WordMetronome.scheduleBeat, 20.0);
       
    }
    
};

jQuery(document).ready(function() 
{
    
    WordMetronome.init(AudioLoader.AUDIO_ENGINE_WEB_AUDIO);
    
});
