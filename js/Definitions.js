/**
 * Word Metronome
 * Copyright 2021 Hamm Software & Samantha Landa
 * https://www.hamm.software
 */
var Definitions = {
    
    'CLICK': 'click',
    'CLICK_HIGH': 'click-high',
    'CLICK_LOW': 'click-low',
    'FILE': 'file',
    
    'defautDefinition': 'animals',
    
    'definitions': {},
    
    'definitionFiles': [
        
        'animals',
        'optimistic',
        'depressing',
        'click',
        'cat',
        
    ],
    
    'countDefinitionsLoaded': 0,
    
    /**
     * Loads the definitions from the specified files.
     * 
     * @author Peter Hamm
     * @param Function callback Optional, a callback function once all the files have been loaded.
     * @return void
     */
    'loadDefinitions': function(callback) 
    {
                
        for(var i = 0; i < Definitions.definitionFiles.length; ++i)
        {
                        
            jQuery.ajax({
                
                'url': WordMetronome.baseUrl + 'js/definitions/' + Definitions.definitionFiles[i] + '.json',
                'type': 'GET',
                
                success: function(data)
                {
                    
                    if(!data.title)
                    {

                        data = JSON.parse(data);

                    }
                                        
                    WordMetronome.log("Loaded the definition '" + data.title + "'.");
                    
                    Definitions.definitions[data.key] = data;

                    ++Definitions.countDefinitionsLoaded;

                    AudioLoader.cacheAllFiles(Definitions.definitions, callback);
                    
                },
                error: function(xhr) 
                {
                    
                    WordMetronome.log("Unable to load a definition!");
                    
                    ++Definitions.countDefinitionsLoaded;
                    
                    AudioLoader.cacheAllFiles(Definitions.definitions, callback);
                    
                }
                
            });
            
        }
        
    },
    
    /**
     * Retrieves whether all definition files have been loaded or not.
     * 
     * @author Peter Hamm
     * @returns bool
     */
    'isLoaded': function()
    {
        
        return Definitions.countDefinitionsLoaded == Definitions.definitionFiles.length;
        
    },
    
    /**
     * Retrieves the configured click data for the specified parameters.
     * 
     * @author Peter Hamm
     * @param String definitionKey
     * @param int signatureNumerator
     * @param int signatureDenominator
     * @return Array
     */
    'getClickForSignatureByIndex': function(definitionKey, signatureIndex)
    {
        
        var definition = Definitions.getDefinition(definitionKey);
        
        if(definition == null || !definition.signatures || definition.signatures.length == 0)
        {
            
            console.error("Invalid definition '" + definitionKey + "'!");
            
            return null;
            
        }
        
        if(definition.signatures.length > signatureIndex && signatureIndex >= 0)
        {
        
            return definition.signatures[signatureIndex].clicks;
            
        }
        
        console.error("Specified signature index #" + signatureIndex + " is out of bounds!");
        
        return null;
        
    },
    
    /**
     * Retrieves the configured click data for the specified parameters.
     * 
     * @author Peter Hamm
     * @param String definitionKey
     * @param int signatureNumerator
     * @param int signatureDenominator
     * @return Array
     */
    'getClickForSignature': function(definitionKey, signatureNumerator, signatureDenominator)
    {
        
        var definition = Definitions.getDefinition(definitionKey);
        
        if(definition == null || !definition.signatures || definition.signatures.length == 0)
        {
            
            console.error("Invalid definition '" + definitionKey + "'!");
            
            return null;
            
        }
        
        for(var i = 0; i < definition.signatures.length; ++i)
        {
                        
            if(definition.signatures[i].numerator == signatureNumerator && definition.signatures[i].denominator == signatureDenominator)
            {
                
                return definition.signatures[i].clicks;
                
            }
            
        }
        
        console.error("Click data for signature " + signatureNumerator + "/" + signatureDenominator + " in '" + definitionKey + "' not found!");
        
        return null;
        
    },
    
    /**
     * Retrieves the definition with the specified key.
     * 
     * @author Peter Hamm
     * @param String definitionKey The key of a definition, typically the filename, e.g. "fun" if the file is "fun.json" in the "definitions" folder.
     * @return Object
     */
    'getDefinition': function(definitionKey)
    {
        
        if(Definitions.definitions[definitionKey])
        {
            
            return Definitions.definitions[definitionKey];
            
        }
        
        return null;
        
    },
    
    /**
     * Retrieves the definition label with the specified key from
     * the specified definition label list (generated by gefDefinitionLabels()).
     * 
     * @author Peter Hamm
     * @param String key
     * @param Array listDefinitionLabels
     * @returns Object
     */
    'getDefinitionLabelForKey': function(key, listDefinitionLabels)
    {
      
        var label = null;
        
        for(var i = 0; i < listDefinitionLabels.length; ++i)
        {
            
            if(listDefinitionLabels[i].key == key)
            {
                
                return listDefinitionLabels[i];
                
            }
            
        }
        
        return label;
        
    },
    
    /**
     * Sorts the specified list of loaded definition labels according
     * to their order defined in Definitions.definitionFiles.
     * 
     * @author Peter Hamm
     * @param Array listDefinitionLabels
     * @returns Array
     */
    'sortDefinitionLabel': function(listDefinitionLabels)
    {
        
        var list = [];
        
        for(var i = 0; i < Definitions.definitionFiles.length; ++i)
        {
            
            var definition = Definitions.getDefinitionLabelForKey(Definitions.definitionFiles[i], listDefinitionLabels);
            
            if(definition != null)
            {
                
                list.push(definition);
                
            }
            
        }
        
        return list;
        
    },
    
    /**
     * Retrieves keys and labels for all loaded definitions.
     * 
     * @author Peter Hamm
     * @return Array
     */
    'getDefinitionLabels': function()
    {
        
        var list = [];
        
        var definitionKeys = Object.keys(Definitions.definitions);
        
        for(var i = 0; i < definitionKeys.length; ++i)
        {
            
            list[i] = {
                
                'key': definitionKeys[i],
                'label': Definitions.definitions[definitionKeys[i]].title
                
            };
            
        }
        
        return Definitions.sortDefinitionLabel(list);
        
    },
    
    /**
     * Retrieves the first definition from the definition stack.
     * 
     * @author Peter Hamm
     * @return Object
     */
    'getFirstDefinitionFromStack': function()
    {
        
        var definitionKeys = Object.keys(Definitions.definitions);
                
        if(definitionKeys.length > 0)
        {
                        
            return Definitions.definitions[definitionKeys[0]];
                        
        }
        
        WordMetronome.log("No definitions loaded yet.");
        
        return null;
        
    }
    
};
