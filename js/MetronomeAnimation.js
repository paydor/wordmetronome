/**
 * Word Metronome
 * Copyright 2021 Hamm Software & Samantha Landa
 * https://www.hamm.software
 */
var MetronomeAnimation = {
    
    'METRONOME_CLASSIC': 'metronome_classic',
    'METRONOME_DISABLED': 'disabled',
    'METRONOME_MODERN': 'metronome_modern',
    'METRONOME_SCREEN_FLASH': 'screen_flash',
    
    'SIDE_LEFT': 'side_left',
    'SIDE_RIGHT': 'side_right',
    
    'nextSide': '',
    
    'currentMetronome': null,
    
    /**
     * Initializes the metronome animation system.
     * 
     * @author Peter Hamm
     * @returns void
     */
    'init': function()
    {
        
        MetronomeAnimation.nextSide = MetronomeAnimation.SIDE_RIGHT;
                
    },
    
    /**
     * Retrieves the container of the animation.
     * 
     * @author Peter Hamm
     * @returns Object
     */
    'getContainer': function()
    {
        
        return jQuery('#metronome-animation');
        
    },
    
    /**
     * Schedules a click animation.
     * 
     * @author Peter Hamm
     * @param int time
     * @returns void
     */
    'scheduleClick': function(time)
    {
                
        if(MetronomeAnimation.currentMetronome == MetronomeAnimation.METRONOME_CLASSIC)
        {
            
            var nextAngle = (MetronomeAnimation.nextSide == MetronomeAnimation.SIDE_RIGHT ? 1 : -1) * 25;

            MetronomeAnimation.nextSide = (MetronomeAnimation.nextSide == MetronomeAnimation.SIDE_RIGHT ? MetronomeAnimation.SIDE_LEFT : MetronomeAnimation.SIDE_RIGHT);

            jQuery('#metronome-classic-pendulum').animateRotate(0, nextAngle, time / 2 * 1000 - 5, 'linear', function() {

                jQuery('#metronome-classic-pendulum').animateRotate(nextAngle, 0, time / 2 * 1000 - 5, 'linear', function () {});

            });
            
        }
        else if(MetronomeAnimation.currentMetronome == MetronomeAnimation.METRONOME_MODERN)
        {
            
            jQuery('#metronome-modern-circle').stop().animate({
                
                'backgroundColor': '#ff0000'
                        
            }, time / 2 * 1000, 'linear', function() {
                
                jQuery('#metronome-modern-circle').stop().animate({

                    'backgroundColor': '#202931'

                }, time / 2 * 1000, 'linear', function() {

                });
                
            });
                        
        }
        else if(MetronomeAnimation.currentMetronome == MetronomeAnimation.METRONOME_SCREEN_FLASH)
        {
            
            jQuery('body').stop().animate({
                
                'backgroundColor': '#ff8000'
                        
            }, time / 2 * 1000, 'linear', function() {
                
                jQuery('body').stop().animate({

                    'backgroundColor': '#202931'

                }, time / 2 * 1000, 'linear', function() {

                });
                
            });
            
        }
        
    },
    
    /**
     * Initializes a classic looking metronome.
     * 
     * @author Peter Hamm
     * @returns void
     */
    'initMetronomeClassic': function()
    {
                
        var metronomeContent = jQuery('<div/>', {
            
            'id': 'metronome-classic-background',
            'class': 'metronome-classic'
            
        })[0].outerHTML;
        
        metronomeContent += jQuery('<div/>', {
            
            'id': 'metronome-classic-pendulum',
            'class': 'metronome-classic'
            
        })[0].outerHTML;
        
        metronomeContent += jQuery('<div/>', {
            
            'id': 'metronome-classic-front',
            'class': 'metronome-classic'
            
        })[0].outerHTML;
        
        MetronomeAnimation.getContainer().html(metronomeContent); 
               
    },
    
    /**
     * Initializes a classic looking metronome.
     * 
     * @author Peter Hamm
     * @returns void
     */
    'initMetronomeModern': function()
    {
                
        var metronomeContent = jQuery('<div/>', {
            
            'id': 'metronome-modern-background',
            'class': 'metronome-modern'
            
        });
        
        metronomeContent.html(jQuery('<div/>', {
            
            'id': 'metronome-modern-circle',
            'class': 'metronome-modern'
            
        })[0].outerHTML);
        
        var metronomeContent = metronomeContent[0].outerHTML;
        
        metronomeContent += jQuery('<i/>', {
            
            'id': 'metronome-modern-icon-play',
            'class': 'fas fa-play'
            
        })[0].outerHTML;
        
        metronomeContent += jQuery('<i/>', {
            
            'id': 'metronome-modern-icon-stop',
            'class': 'fas fa-stop'
            
        })[0].outerHTML;
        
        MetronomeAnimation.getContainer().html(metronomeContent); 
               
    },
    
    'onMetronomePlay': function()
    {
        
        jQuery('#metronome-animation').addClass('metronome-animation-playing');
                
    },
    
    'onMetronomeStop': function()
    {
        
        setTimeout(function() {
            
            jQuery('#metronome-animation').removeClass('metronome-animation-playing');
            
        }, 100);
                
    },
    
    /**
     * Handles changes of the metronome style.
     * For example: Adds extra CSS classes so the metronome can look differently depending on the selected style.
     * 
     * @author Peter Hamm
     * @returns void
     */
    'onStyleChange': function()
    {
        
        var newStyle = WordMetronome.getSelectedStyle();
        
        if(MetronomeAnimation.currentMetronome == MetronomeAnimation.METRONOME_CLASSIC)
        {
            
            jQuery('#metronome-classic-background').attr('class', 'metronome-classic').addClass('metronome-classic-background-' + newStyle);
            jQuery('#metronome-classic-pendulum').attr('class', 'metronome-classic').addClass('metronome-classic-pendulum-' + newStyle);
            jQuery('#metronome-classic-front').attr('class', 'metronome-classic').addClass('metronome-classic-front-' + newStyle);
            
        }
        
    },
    
    /**
     * Handles changes of the selected metronome.
     * 
     * @author Peter Hamm
     * @returns void
     */
    'onMetronomeChange': function()
    {
                
        var newMetronome = jQuery('[name="selector-animation"]:checked').val();
                
        MetronomeAnimation.currentMetronome = newMetronome;
        
        jQuery('#metronome-animation').css('display', newMetronome == MetronomeAnimation.METRONOME_DISABLED || newMetronome == MetronomeAnimation.METRONOME_SCREEN_FLASH ? 'none' : 'block');
        
        jQuery('#row-btn-play-stop').css('display', newMetronome == MetronomeAnimation.METRONOME_MODERN ? 'none' : 'block');
        
        if(newMetronome == MetronomeAnimation.METRONOME_CLASSIC)
        {
        
            MetronomeAnimation.initMetronomeClassic();
            
        } 
        else if(newMetronome == MetronomeAnimation.METRONOME_MODERN)
        {
        
            MetronomeAnimation.initMetronomeModern();
            
        } 
       
    }
    
};

jQuery.fn.animateRotate = function(startAngle, endAngle, duration, easing, callback)
{
    
    return this.each(function()
    {
        
        var element = jQuery(this);

        element.stop().animate({
            
            deg: endAngle
        
        }, {
            
            duration: duration,
            easing: easing,
            
            step: function(now) {
                
                element.css({
                    
                  '-moz-transform':'rotate(' + now + 'deg)',
                  '-webkit-transform':'rotate(' + now + 'deg)',
                  '-o-transform':'rotate(' + now + 'deg)',
                  '-ms-transform':'rotate(' + now + 'deg)',
                  'transform':'rotate(' + now + 'deg)'
                  
                });
                
            },
            
            complete: callback || jQuery.noop
            
        });
        
    });
    
};

jQuery.fn.animateScaleOpacity = function(scale, opacity, duration, easing, callback)
{
    
    return this.each(function()
    {
        
        var element = jQuery(this);

        element.stop().animate({
            
            'scale': scale, 
            'opacity': opacity
        
        }, {
            
            duration: duration,
            easing: easing,
            
            step: function(now) {
                
                element.css({
                  
                    'opacity': now
                  
                });
                
            },
            
            complete: callback || jQuery.noop
            
        });
        
    });
    
};
